//
//  SpeechRecognitionManagerBridge.m
//  AxysCodeSample
//
//  Created by Jon Cox on 21/03/2017.
//  Copyright © 2017 Facebook. All rights reserved.
//

// SpeechRecognitionManagerBridge.m
#import "React/RCTBridgeModule.h"


@interface RCT_EXTERN_MODULE(SpeechRecognitionManager, NSObject)

RCT_EXTERN_METHOD(requestPermission: (RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(startRecording: (RCTResponseSenderBlock)onError)

RCT_EXTERN_METHOD(stopRecording)

@end
