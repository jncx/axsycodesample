//
//  SpeechRecognitionManager.swift
//  MiniMonsters
//
//  Created by Jon Cox on 07/10/2016.
//  Copyright © 2016 Facebook. All rights reserved.
//
import Foundation
import Speech

let NOTIFICATION_RECEIVED = "SpeechRecognizer.SpeechReceived"
let NOTIFICATION_STARTED = "SpeechRecognizer.Started"
let NOTIFICATION_STOPPED = "SpeechRecognizer.Stopped"

@available(iOS 10.0, *)
@objc(SpeechRecognitionManager)
class SpeechRecognitionManager: RCTEventEmitter {
  
  //  private var speechRecognizer = SFSpeechRecognizer()
  private let audioEngine = AVAudioEngine()
  private let speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: "en_GB"))!
  
  private var recognitionTask: SFSpeechRecognitionTask?
  private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
  
  /* required by RCTEventEmittier */
  @objc override func supportedEvents() -> [String]! {
    return [NOTIFICATION_STARTED, NOTIFICATION_RECEIVED, NOTIFICATION_STOPPED]
  }
  
  @objc func requestPermission(_ callback: @escaping ([Any]) -> ()) -> Void {
    SFSpeechRecognizer.requestAuthorization { authStatus in
      /*
       Request authorization to use the speech recognition.
       
       Returns an object:
       isAuthorized: boolean
       message: string
       */
      var isAuthorized: Bool
      var message: String
      switch authStatus {
      case .authorized:
        isAuthorized = true
        message = "Authorized"
      case .denied:
        isAuthorized = false
        message = "User denied access to speech recognition"
      case .restricted:
        isAuthorized = false
        message = "Speech recognition restricted on this device"
      case .notDetermined:
        isAuthorized = false
        message = "Speech recognition not yet authorized"
      }
      
      callback([[
        "isAuthorized": isAuthorized,
        "message": message
        ]])
      
    }
  }
  
  
  @objc func startRecording(_ onError: @escaping ([Any]) -> ()) -> Void {
    
    // Cancel the previous task if it's running.
    if let recognitionTask = recognitionTask {
      print("audioEngine cancel the previous task")
      recognitionTask.cancel()
      self.recognitionTask = nil
    }
    
    let audioSession = AVAudioSession.sharedInstance()
    do {
      try audioSession.setCategory(AVAudioSessionCategoryRecord)
      try audioSession.setMode(AVAudioSessionModeMeasurement)
      try audioSession.setActive(true, with: .notifyOthersOnDeactivation)
    } catch {
      print("audioSession properties weren't set because of an error.")
    }
    recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
    
    guard let inputNode = audioEngine.inputNode else { fatalError("Audio engine has no input node") }
    guard let recognitionRequest = recognitionRequest else { fatalError("Unable to created a SFSpeechAudioBufferRecognitionRequest object") }
    
    // Configure request so that results are returned before audio recording is finished
    recognitionRequest.shouldReportPartialResults = true
    
    // A recognition task represents a speech recognition session.
    // We keep a reference to the task so that it can be cancelled.
    recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest) { result, error in
      var isFinal = false
      var transcript = ""
      
      if let result = result {
        transcript = result.bestTranscription.formattedString
        isFinal = result.isFinal
      }
      
      if error != nil || isFinal {
        
        self.audioEngine.stop()
        //        inputNode.removeTap(onBus: 0)
        
        self.recognitionRequest = nil
        self.recognitionTask = nil
      }
      if error != nil {
        print("audioEngine error: \(error)")
      } else {
        print("audioEngine transcript: \(transcript)")
      }
      self.sendEvent(withName: NOTIFICATION_RECEIVED, body: [
        "isFinal": isFinal,
        "transcript": transcript,
        "error": "\(error)"
        ])
    }
    
    let recordingFormat = inputNode.outputFormat(forBus: 0)
    inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer: AVAudioPCMBuffer, when: AVAudioTime) in
      self.recognitionRequest?.append(buffer)
    }
    
    audioEngine.prepare()
    
    do {
      try audioEngine.start()
      print("audioEngine started")
    } catch {
      print("error starting audioEngine")
    }
    
  }
  
  @objc func stopRecording() -> Void {
    
    self.audioEngine.stop()
    
    guard let inputNode = self.audioEngine.inputNode else { fatalError("Audio engine has no input node") }
    inputNode.removeTap(onBus: 0)
    
    self.audioEngine.reset()
    self.recognitionRequest?.endAudio()
    
    self.recognitionRequest = nil
    self.recognitionTask = nil
    
    self.sendEvent(withName: NOTIFICATION_STOPPED, body: [])
    
    print("audioEngine stopped.")
    
  }
  
}
