import {
  AppRegistry,
} from 'react-native';
import Reader from './app/main';

AppRegistry.registerComponent('AxysCodeSample', () => Reader);