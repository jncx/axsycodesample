Code Sample for Axsy
====================

### Introduction ###

This is a sample app I've put together to demonstrate a recent project I've been working on. It's been
stripped out of an app currently in-development and solely worked on by me, so it contains logging
statements and the comments aren't extensive, so apologies for this. It is in no means revolutionary,
but it demonstrates my use of es6, coding style, use of redux and async actions with redux-thunk,
implementing and consuming custom Swift modules.

The app utilises [iOS 10's Speech Recognition API](https://developer.apple.com/reference/speech) so
is therefore *iOS only* and requires access to the microphone. *Running on the simulator or Android
will result in a crash!*.

The following is a brief description of files of note:

#### ios/SpeechRecognitionManager.swift ####
As the speech API is not yet implemented in react-native, this file provides a wrapper which we
can call in JS. It's basically the sample code taken from the documentation, except that I've
subclassed `RCTEventEmitter` so we have access to the react event system. This allows us to subscribe
to events emitted from the class so we can track the recording status and transcripts returned by the
API.

#### ios/SpeechRecognitionManagerBridge.m ####
The objective-c bridging header, exposing Swift modules to react-native.

#### app/services/speechService.js ####
This class allows components to manage subscriptions to the `SpeechRecognitionManager` and also
controls starting and stopping of the service.

#### app/components/speechRecognition/components/speechRecognition.js ####
The main component that uses the `SpeechService` class to start/stop speech recognition and subscribe
callbacks to the speech events from which the state is updated via redux.


### Getting started ###

Clone, install dependencies & run (assumes npm and XCode installed)::

    $ git clone git@bitbucket.org:jncx/axsycodesample.git ./AxsyCodeSample
    $ cd AxsyCodeSample
    $ npm install
    $ react-native run-ios

### Questions ###
Any questions please contact me at jon@non-scratch.com