import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';

class NavigationBar extends Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }

    _renderLeftView() {
        if (this.props.leftView) {
            return this.props.leftView;
        } else if (this.props.navigator.getCurrentRoutes().length > 1) {
            // render back button by default
            return (
                <TouchableOpacity
                    style={styles.headerItem}
                    onPress={() => { this.props.navigator.pop(); }}
                >
                    <Text>{this.props.backButtonLabel}</Text>
                </TouchableOpacity>
            );
        }
        return null;
    }

    render() {
        return (<View style={styles.headerRow}>
                <View style={styles.headerItem}>
                    {this._renderLeftView()}
                </View>
                <View style={styles.headerItem, styles.headerStretchItem}>
                    {this.props.children}
                </View>
                <View style={[styles.headerItem, styles.headerRight]}>
                    {this.props.rightView}
                </View>
            </View>);
    }
}

NavigationBar.propTypes = {
    navigator: React.PropTypes.object.isRequired,
    // leftView: React.PropTypes.object.isOptional,
    // rightView: React.PropTypes.object.isOptional,
    // backButtonLabel: React.PropTypes.string.isOptional,
};

NavigationBar.defaultProps = {
    backButtonLabel: 'Back',
};

const styles = StyleSheet.create({
    headerRow: {
        flexDirection: 'row',
        backgroundColor: 'whitesmoke',
        height: 40,
        padding: 10,
    },
    headerItem: {
        marginRight: 4,
        width: 80,
    },
    headerStretchItem: {
        flex: 1,
        alignItems: 'center',
    },
    headerRight: {
        alignItems: 'flex-end',
        paddingRight: 10,
    },
});


module.exports = NavigationBar;
