// app constants
// -------------

module.exports = {

    COLOR_GREY: '#F8F9FA',
    COLOR_BLUE: '#7adbf6',
    COLOR_KNOWN: '#4BDEFB',
    COLOR_NOT_KNOWN: '#FFBDD4',
    COLOR_NOT_SEEN: '#F1F1F1',
    COLOR_DAILY: '#ffe5c4',
    COLOR_DAILY_TARGET: '#67e3dd',

};
