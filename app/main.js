import React, { Component } from 'react';
import { Navigator, StyleSheet, View } from 'react-native';
import {
    Provider,
} from 'react-redux';
import store from './core/store';

import { SpeechRecognitionTest } from './screens/speechRecognition';
import { BatteryStatus } from './screens/batteryStatus';

export default class App extends Component {

    configureScene() {
        return {
          ...Navigator.SceneConfigs.PushFromRight,
          gestures: {}, // disable gestures for navigation
        }
    }

    renderScene = (route, navigator) => {
        if (route.render) {
            return (route.render());
        }
        switch (route.index) {
            case 1:
                return (<BatteryStatus navigator={navigator} />);
            default:
                return (<SpeechRecognitionTest navigator={navigator} />);
        }
    }

    render() {
        return (
            <Provider store={store}>
                <View style={styles.background}>
                    <Navigator
                        style={styles.navigator}
                        initialRoute={{
                            name: "Initial scene",
                            index: 0
                        }}
                        renderScene={this.renderScene}
                        configureScene={this.configureScene}/>
                </View>
            </Provider>
        );
    }
}

var styles = StyleSheet.create({
    background: {
        backgroundColor:"white",
        paddingTop: 20,
        flex:1,
        width:null,
        height:null
    },
    navigator: {
        flex:1,
        backgroundColor:"transparent"
    }
});
