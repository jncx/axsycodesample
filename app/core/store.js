import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';

// import logger from './logger';
import reducers from './reducers';

const store = createStore(reducers, applyMiddleware(
    thunkMiddleware, // allows us to dispatch() functions for async action types
    // logger
  ));

export default store;
