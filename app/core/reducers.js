import { combineReducers } from 'redux';

import { reducers as speechRecognition } from '../screens/speechRecognition';
import { reducers as batteryStatus } from '../screens/batteryStatus';

const reducers = combineReducers({
    batteryStatus,
    speechRecognition,
});

export default reducers;
