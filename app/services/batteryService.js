import {
    DeviceEventEmitter,
    NativeModules,
 } from 'react-native';
const BatteryManager = NativeModules.BatteryManager;

export const NOTIFICATION_NAME = 'BatteryStatus';

class BatteryService {

    updateBatteryLevel(callback) {
        BatteryManager.updateBatteryLevel(callback);
    }

    subscribe(callback) {
        /* subscribe caller to battery manager events events */
        return DeviceEventEmitter.addListener(
            NOTIFICATION_NAME,
            callback
        );
    }

    unsubscribe(subscription) {
        subscription.remove();
    }

}
export default new BatteryService();
