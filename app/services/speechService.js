import {
    NativeEventEmitter,
    NativeModules,
 } from 'react-native';
const Manager = NativeModules.SpeechRecognitionManager;

export const NOTIFICATION_SPEECH_RECEIVED = 'SpeechRecognizer.SpeechReceived';
export const NOTIFICATION_STARTED = 'SpeechRecognizer.Started';
export const NOTIFICATION_STOPPED = 'SpeechRecognizer.Stopped';

class SpeechService {

    requestPermission(callback) {
        Manager.requestPermission((authStatus) => {
            console.log('requestPermission callback', authStatus);
            callback(authStatus);
        });
    }

    start(callback) {
        Manager.startRecording(
            (error) => {
                console.log('startRecording onError', error);
                callback(error);
            }
        );
        callback();
    }

    stop(callback) {
        Manager.stopRecording();
        callback();
    }

    subscribe(notification, callback) {
        /* subscribe caller to speech recognition events */
        const speechModule = new NativeEventEmitter(NativeModules.SpeechRecognitionManager);
        return speechModule.addListener(
            notification,
            callback
        );
    }

    unsubscribe(subscription) {
        subscription.remove();
    }

}
export default new SpeechService();
