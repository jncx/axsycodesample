
import React from 'react';

import { createActionCreator }  from '../../core/actionCreator';

import BatteryService from '../../services/batteryService';

/* Constants */
export const STATUS_UPDATE = 'STATUS_UPDATE';

/* actions */
export const updateBatteryStatus = createActionCreator(STATUS_UPDATE, 'status',);