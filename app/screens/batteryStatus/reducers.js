
const Actions = require('./actions');

const defaultState = {
    level: 0,
    isCharging: false,
};


function reducers(state = defaultState, action) {
    switch (action.type) {

    case Actions.STATUS_UPDATE:
        return { ...state,
                level: action.status.level || 0,
                isCharging: action.status.isPlugged,
              };

    default:
        return state;
    }

}

export default reducers;
