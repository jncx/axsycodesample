import { connect } from 'react-redux';
import {
    updateBatteryStatus,
} from '../actions';
import BatteryStatus from '../components/batteryStatus';

const mapStateToProps = (state) => {
    const s = state.batteryStatus;
    return {
        level: s.level,
        isCharging: s.isCharging,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateBatteryStatus: (status) => {
            dispatch(updateBatteryStatus(status));
        },
    };
};

const BatteryStatusContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(BatteryStatus);

export default BatteryStatusContainer;