import React, { Component } from 'react';
import {
    Animated,
    LayoutAnimation,
    StyleSheet,
    View,
} from 'react-native';


class BatteryIndicator extends Component {

    constructor(props) {
        super(props);

        this.state = {
            width: 0,
            chargingWidth: new Animated.Value(0)
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.level !== this.props.level) {
            this.setState({
                chargingWidth: new Animated.Value(this.state.width * (nextProps.level / 100))
            });
        }
    }

    componentWillUpdate() {
        LayoutAnimation.easeInEaseOut();
    }

    animateCharging() {
        if (this.props.isCharging) {
            Animated.timing(this.state.chargingWidth, {
                toValue: this.state.width,
                duration: 900,
            }).start(() => {
                this.state.chargingWidth.setValue(this.state.width * (this.props.level / 100))
                this.animateCharging();
            });
        }
    }

    render() {
        let widthStyle;
        if (this.state.width) {
            if (this.props.isCharging) {
                widthStyle = {
                    maxWidth: this.state.width,
                    width: this.state.chargingWidth,
                }
                this.animateCharging();

            } else {
                // current battery level
                widthStyle = {
                    maxWidth: this.state.width * (this.props.level / 100)
                }
            }

        }

        return (
            <View
                style={styles.container}
                onLayout={(event) => {
                    this.setState({
                        width: event.nativeEvent.layout.width,
                    });
                }}>
                <Animated.View style={[styles.levelIndicator, widthStyle]} />
            </View>
        );
    }
}

BatteryIndicator.propTypes = {
    level: React.PropTypes.number.isRequired,
    isCharging: React.PropTypes.bool.isRequired,
};

BatteryIndicator.defaultProps = {
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignSelf: 'center',
        borderWidth: 4,
        width: 300,
        maxHeight: 100,
    },
    levelIndicator: {
        flex: 1,
        margin: 4,
        backgroundColor: 'red',
    },
});
module.exports = BatteryIndicator;
