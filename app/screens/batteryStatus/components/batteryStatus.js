import React, { Component } from 'react';
import {
    Slider,
    StyleSheet,
    Switch,
    Text,
    View,
} from 'react-native';

import BatteryService from '../../../services/batteryService';

import NavigationBar from '../../../components/navigationBar';

import BatteryIndicator from './batteryIndicator';

class BatteryStatus extends Component {

    constructor(props) {
        super(props);

        this.state = {
            currentStatus: 0
        }

        // get initial battery status and then subscribe to any changes
        BatteryService.updateBatteryLevel((status) => {
            console.log('battery status', status);
            this.props.updateBatteryStatus(status);
            this.subscription = BatteryService.subscribe(this.props.updateBatteryStatus);
        })


    }

    componentWillUnmount() {
        BatteryService.unsubscribe(this.subscription);
    }

    renderNavBar() {
        return (<NavigationBar
                    navigator={this.props.navigator}
                    >
                    <Text>Battery Status</Text>
                </NavigationBar>);
    }

    render() {
        return (
            <View style={styles.container}>
                {this.renderNavBar()}

                <View style={styles.body}>
                    <BatteryIndicator
                        level={this.props.level}
                        isCharging={this.props.isCharging}
                    />

                    <View style={styles.labels}>
                        <Text>Current level: {this.props.level}</Text>
                        <Text>{this.props.isCharging ? 'Charging' : 'Not plugged in'}</Text>
                    </View>

                    <View style={styles.controls}>
                        <Slider
                            value={this.props.level}
                            onValueChange={(value) =>
                                this.props.updateBatteryStatus({
                                    level: parseInt(value * 100),
                                    isPlugged: this.props.isCharging,
                                })
                            }
                        />

                        <Switch
                            onValueChange={(value) =>
                                this.props.updateBatteryStatus({
                                    level: this.props.level,
                                    isPlugged: !this.props.isCharging,
                                })
                            }
                            value={this.props.isCharging}
                        />

                    </View>
                </View>
            </View>
        );
    }
}

BatteryStatus.propTypes = {
    level: React.PropTypes.number.isRequired,
    isCharging: React.PropTypes.bool.isRequired,

    updateBatteryStatus: React.PropTypes.func.isRequired,
};

BatteryStatus.defaultProps = {
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
    },
    body: {
        flex: 1,
        justifyContent: 'center',
    },
    labels: {
        margin: 40,
        alignSelf: 'center',
    },
    controls: {
        margin: 20,
    }
});
module.exports = BatteryStatus;
