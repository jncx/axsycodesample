import SpeechRecognitionContainer from './containers/speechRecognitionContainer';
import SpeechRecognitionTest from './components/speechRecognitionTest';
import reducers from './reducers';

exports.SpeechRecognitionTest = SpeechRecognitionTest;
exports.SpeechRecognition = SpeechRecognitionContainer;
exports.reducers = reducers;
