
const Actions = require('./actions');

const defaultState = {
    transcript: 'Press button to speak',
    isRecording: false,
};


function reducers(state = defaultState, action) {
    switch (action.type) {

    case Actions.STATE_UPDATE:
        const newState = {};
        newState[action.key] = action.value;
        const update = { ...state,
                ...newState,
            }
        return update;

    default:
        return state;
    }
}

export default reducers;
