import React, { Component } from 'react';
import {
    // Animated,
    StyleSheet,
    Text,
    TouchableHighlight,
    View,
} from 'react-native';

import constants from '../../../conf/constants';

import SpeechService, {
    NOTIFICATION_STARTED,
    NOTIFICATION_STOPPED,
    NOTIFICATION_SPEECH_RECEIVED,
} from '../../../services/speechService';

class SpeechRecognition extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isActive: false,
        };

        /* subscribe to speech recognition events */
        console.log('subscribe ...', NOTIFICATION_SPEECH_RECEIVED);
        this.onSpeechReceived = SpeechService.subscribe(NOTIFICATION_SPEECH_RECEIVED,
            (response) => {
                console.log('event onSpeechReceived', response);
                if (response.transcript) {
                    this.props.updateState('transcript', response.transcript);
                } else if (response.error) {
                    this.props.updateState('transcript', 'Error with speech api');
                    console.warn(response.error);
                }
            }
        );

        this.onSpeechStarted = SpeechService.subscribe(NOTIFICATION_STARTED,
            () => {
                // TODO: nothing yet...
                console.log('event onSpeechStarted');
                this.props.updateState('isRecording', true);
            }
        );

        this.onSpeechStopped = SpeechService.subscribe(NOTIFICATION_STOPPED,
            () => {
                console.log('event onSpeechStopped');
                if (this.props.isRecording) {
                    // user is recording, so attempt restart recording
                    this.props.startRecording();
                }
            }
        );
    }

    componentDidUnMount() {
        // this.subscription.remove();
        SpeechService.unsubscribe(this.onSpeechReceived);
        SpeechService.unsubscribe(this.onSpeechStarted);
        SpeechService.unsubscribe(this.onSpeechStopped);
    }

    renderResults() {
        return (
            <View style={styles.resultsContainer}>
                <Text
                    ellipsizeMode="tail"
                    numberOfLines={1}
                    style={styles.result}
                >{this.props.transcript.length > 25
                    ? this.props.transcript.substr(this.props.transcript.length - 25)
                    : this.props.transcript}</Text>
            </View>
        );
    }

    render() {
        return (
            <View style={styles.container}>
                {this.renderResults()}
                <TouchableHighlight
                    style={[styles.button, this.props.isRecording ? styles.on : styles.off]}
                    underlayColor={constants.COLOR_DAILY}
                    onPressIn={() => {
                        if (!this.props.isRecording) {
                            this.props.startRecording();
                        }
                    }}
                    onPressOut={() => {
                        // if (this.props.isRecording) {
                            this.props.stopRecording();
                            this.props.updateState('isRecording', false);
                        // }
                    }}
                >
                    <View style={styles.buttonSpaceHolder}/>
                </TouchableHighlight>
            </View>
        );
    }
}

SpeechRecognition.propTypes = {
    isRecording: React.PropTypes.bool.isRequired,
    transcript: React.PropTypes.string.isRequired,
    startRecording: React.PropTypes.func.isRequired,
    stopRecording: React.PropTypes.func.isRequired,
    updateState: React.PropTypes.func.isRequired,
};

SpeechRecognition.defaultProps = {
};

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        margin: 20,
    },
    button: {
        padding: 24,
        backgroundColor: constants.COLOR_DAILY,
        borderRadius: 400,
        marginLeft: -10,
        marginTop: -22,
    },
    on: {
        backgroundColor: constants.COLOR_DAILY,
    },
    off: {
        backgroundColor: 'whitesmoke',
    },
    buttonSpaceHolder: {
        width: 40,
        height: 40,
    },
    resultsContainer: {
        flex: 1,
        backgroundColor: 'lightgray',
        paddingRight: 20,
        borderTopLeftRadius: 20,
        borderBottomLeftRadius: 20,
        height: 40,
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
    result: {
        color: 'white',
    },
});
module.exports = SpeechRecognition;
