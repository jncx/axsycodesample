import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';

import SpeechService, { NOTIFICATION_SPEECH_RECEIVED } from '../../../services/speechService';

import NavigationBar from '../../../components/navigationBar';

import { SpeechRecognition } from '../';

class SpeechRecognitionTest extends Component {

    constructor(props) {
        super(props);
        this.state = {
            transcript: 'Press start to detect speech',
        };

        /* subscribe to speech recognition events */
        this.subscription = SpeechService.subscribe(NOTIFICATION_SPEECH_RECEIVED,
            (response) => {
                this.setState({
                    transcript: response.transcript,
                });
            }
        );
    }

    componentDidUnMount() {
        SpeechService.unsubscribe(this.subscription);
    }

    renderNavBar() {
        return (<NavigationBar
                    navigator={this.props.navigator}
                    rightView={
                        <TouchableOpacity
                            style={styles.headerItem}
                            onPress={() => {
                                this.props.navigator.push({
                                    name: "Battery Status",
                                    index: 1
                                });
                            }}
                        >
                            <Text>Battery</Text>
                        </TouchableOpacity>
                    }
                    >
                    <Text>Speech Recognition</Text>
                </NavigationBar>);
    }

    renderResults() {
        return (
            <View style={styles.resultsContainer}>
                <Text>{this.state.transcript}</Text>
            </View>
        );
    }

    render() {
	    return (
            <View style={styles.container}>
                 {this.renderNavBar()}
                <View style={styles.body}>
                    {this.renderResults()}
                    <SpeechRecognition />

                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: null,
        height: null,
        backgroundColor: 'white',
    },
    resultsContainer: {
        flex: 1,
        padding: 40,
    },
    body: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    label: {
        fontSize: 20,
        margin: 20,
    },
    button: {
        padding: 20,
        alignSelf: 'center',
        backgroundColor: 'whitesmoke',
        borderRadius: 100,
    },
    result: {
        padding: 20,
    }
});
module.exports = SpeechRecognitionTest;
