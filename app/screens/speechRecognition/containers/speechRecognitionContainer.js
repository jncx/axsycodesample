import { connect } from 'react-redux';
import {
    startRecording,
    stopRecording,
    updateState,
} from '../actions';
import SpeechRecognition from '../components/speechRecognition';

const mapStateToProps = (state) => {
    const s = state.speechRecognition;
    return {
        transcript: s.transcript,
        isRecording: s.isRecording,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        startRecording: () => {
            dispatch(startRecording());
        },
        stopRecording: () => {
            dispatch(stopRecording());
        },
        updateState: (name, value) => {
            dispatch(updateState(name, value));
        },
    };
};

const SpeechRecognitionContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true }
)(SpeechRecognition);

export default SpeechRecognitionContainer;
