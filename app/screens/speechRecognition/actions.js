import React from 'react';

import { createActionCreator }  from '../../core/actionCreator';

import SpeechService from '../../services/speechService';

/* Constants */
export const STATE_UPDATE = 'STATE_UPDATE';

export function startRecording() {
    return dispatch =>
    SpeechService.requestPermission((authStatus) => {
        console.log('authStatus');
        // SpeechService.start(() => {
        //     // isRecording state should now be updated in started/stopped event handlers
        //     // dispatch(updateState('isRecording', true));
        // });
    });
}

export function stopRecording() {
    return dispatch =>
    SpeechService.stop(() => {
        // isRecording state should now be updated in started/stopped event handlers
        // dispatch(updateState('isRecording', false));
    });
}

export const updateState = createActionCreator(STATE_UPDATE, 'key', 'value');
